﻿# language: ru

@СчетФактураВыданный
@tree
@smoke

Функционал: Тестирование открытия форм для подсистемы 
	Как Разработчик
	Я Хочу чтобы проверялось открытие формы всех элементов этой подсистемы
	Чтобы я мог гарантировать работоспособность заполнения форм на основании

Сценарий: Ввод на основании документа "СчетФактураВыданный"
		Когда Я открываю форму документа "УдалитьПроизвольныйЭД" заполненного на основании проведенного "СчетФактураВыданный"
